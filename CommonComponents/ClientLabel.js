
import React, { Component } from 'react';
import { Text, ImageBackground, TouchableOpacity, StyleSheet, View } from 'react-native';
// import MyStyle from '../../Styles/MyStyle';
var MyStyle = require('../Styles/MyStyle');

export class ClosedLable extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView, { borderColor: 'lightgreen' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt]}>Closed</Text>
            </View>
        )
    }
}
export class ClosedLableClient extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView_Client, { borderColor: 'lightgreen' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt]}>Closed</Text>
            </View>
        )
    }
}


export class ExpiredLable extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView, { borderColor: 'rgba(104,159,213, .87)' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: 'rgba(104,159,213, .87)' }]}>Expired</Text>
            </View>
        )
    }
}

export class ExpiredLableClient extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView_Client, { borderColor: 'rgba(104,159,213, .87)' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: 'rgba(104,159,213, .87)' }]}>Expired</Text>
            </View>
        )
    }
}
// 
export class PostponedLable extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView, { borderColor: 'rgba(104,159,213, .87)' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: 'rgba(104,159,213, .87)' }]}>Postponed</Text>
            </View>
        )
    }
}

export class PostponedLableClient extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView_Client, { borderColor: 'rgba(104,159,213, .87)' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: 'rgba(104,159,213, .87)' }]}>Postponed</Text>
            </View>
        )
    }
}

// 
export class InContractLable extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView, { borderColor: 'rgba(104,159,213, .87)' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: 'rgba(104,159,213, .87)' }]}>In Contract</Text>
            </View>
        )
    }
}

export class InContractLableClient extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView_Client, { borderColor: 'rgba(104,159,213, .87)' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: 'rgba(104,159,213, .87)' }]}>In Contract</Text>
            </View>
        )
    }
}
export class SoldLable extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView, { borderColor: 'rgba(104,159,213, .87)' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: 'rgba(104,159,213, .87)' }]}>Sold</Text>
            </View>
        )
    }
}

export class SoldLableClient extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView_Client, { borderColor: 'rgba(104,159,213, .87)' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: 'rgba(104,159,213, .87)' }]}>Sold</Text>
            </View>
        )
    }
}

export class DeclinedLable extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView, { borderColor: 'rgba(104,159,213, .87)' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: 'rgba(104,159,213, .87)' }]}>Declined</Text>
            </View>
        )
    }
}
export class DeclinedLableClient extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView_Client, { borderColor: 'rgba(104,159,213, .87)' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: 'rgba(104,159,213, .87)' }]}>Declined</Text>
            </View>
        )
    }
}

export class SubmittedLable extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView, { borderColor: '#8ed0bd' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxtSubmit, { color: '#8ed0bd' }]}>Submitted</Text>
            </View>
        )
    }
}

export class SubmittedLableClient extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView_Client, { borderColor: '#8ed0bd' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: '#8ed0bd' }]}>Submitted</Text>
            </View>
        )
    }
}

// f7c698  for= won/listed/match
// 8ab8dd  for => sold, in contract
export class WonLable extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView, { borderColor: '#f1aa62' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: '#f1aa62' }]}>Won</Text>
            </View>
        )
    }
}

export class WonLableClient extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView_Client, { borderColor: '#f1aa62' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: '#f1aa62' }]}>Won</Text>
            </View>
        )
    }
}

export class ListedLable extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView, { borderColor: '#f1aa62' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: '#f1aa62' }]}>Listed</Text>
            </View>
        )
    }
}

export class ListedLableClient extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView_Client, { borderColor: '#f1aa62' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: '#f1aa62' }]}>Listed</Text>
            </View>
        )
    }
}

export class ExpiresSoonLable extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView, { borderColor: '#8ed0bd' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: '#8ed0bd' }]}>Expires Soon</Text>
            </View>
        )
    }
}
export class ExpiresSoonLableClient extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView_Client, { borderColor: '#8ed0bd' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: '#8ed0bd' }]}>Expires Soon</Text>
            </View>
        )
    }
}


export class InterviewingLable extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView, { borderColor: '#ffc950' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: '#ffc950' }]}>Interviewing</Text>
            </View>
        )
    }
}

export class InterviewingLableClient extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView_Client, { borderColor: '#ffc950' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: '#ffc950' }]}>Interviewing</Text>
            </View>
        )
    }
}



export class InvitedLable extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView, { borderColor: '#8ed0bd' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: '#8ed0bd' }]}>Invited</Text>
            </View>
        )
    }
}
export class InvitedLableClient extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView_Client, { borderColor: '#8ed0bd' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: '#8ed0bd' }]}>Invited</Text>
            </View>
        )
    }
}

export class PendingLable extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView, { borderColor: '#8ed0bd' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: '#8ed0bd' }]}>Pending</Text>
            </View>
        )
    }
}
export class PendingLableClient extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView_Client, { borderColor: '#8ed0bd' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: '#8ed0bd' }]}>Pending</Text>
            </View>
        )
    }
}

export class MatchedLable extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView, { borderColor: '#8ed0bd' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: '#8ed0bd' }]}>Matched</Text>
            </View>
        )
    }
}

export class MatchedLableClient extends Component {
    render() {
        return (
            <View style={[MyStyle.pendingView_Client, { borderColor: '#8ed0bd' }]}>
                <Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt, { color: '#8ed0bd' }]}>Matched</Text>
            </View>
        )
    }
}
