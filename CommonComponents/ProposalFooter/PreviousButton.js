//@flow
'use-strict'


import React, { Component } from 'react';
import { Text, ImageBackground, TouchableOpacity, StyleSheet, View, Dimensions } from 'react-native';
import Prepare_proposal_Styles from '../../Styles/Prepare_proposal_Styles';
const Window_width = Dimensions.get('window').width;
const Window_height = Dimensions.get('window').height;


export default class PreviousButton extends Component {

    props: {
        title: string,
        // titleStyle?: mixed,
        // color: string,
        onPress: func,
        style: mixed
    }

    render() {


        return (

            <View style={{ flex: 1 }}>
                <TouchableOpacity onPress={this.props.onPress}>
                    <View style={styles.previous_button_Style}>
                        <Text style={styles.previous_text_Style}>{this.props.title}</Text>
                    </View>
                </TouchableOpacity>
            </View>

        )
    }
}


const styles = StyleSheet.create({
    previous_text_Style:
    {
        color: 'black',
        fontSize: 14,
        textAlign: 'center',
    },
    previous_button_Style:
    {
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        borderRadius: 25,
        backgroundColor: '#EBEBEC',
        position: 'absolute',
        left: 0
    },
})