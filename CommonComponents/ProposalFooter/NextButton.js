//@flow
'use-strict'


import React, { Component } from 'react';
import { Text, ImageBackground, TouchableOpacity, StyleSheet, View } from 'react-native';
import Prepare_proposal_Styles from '../../Styles/Prepare_proposal_Styles';

export default class NextButton extends Component {

    props: {
        titleBack: string,
        titleNext: string,
        titleStyle?: mixed,
        // color: string,
        onPressBack: func,
        onPressNext: func,
        style: mixed
    }

    render() {

        return (

            <View style={{ position: 'absolute', bottom: 0, backgroundColor: '#f6f6f9' }}>
                <View style={Prepare_proposal_Styles.footerBar}>
                    <View style={Prepare_proposal_Styles.insideFooterBar}>

                        <TouchableOpacity onPress={this.props.onPressBack}>
                            <View style={Prepare_proposal_Styles.previous_button_Style}>
                                <Text style={Prepare_proposal_Styles.previous_text_Style}>{this.props.titleBack}</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={this.props.onPressNext}>
                            <View style={Prepare_proposal_Styles.next_button_Style}>
                                <Text style={[Prepare_proposal_Styles.next_text_Style, this.props.titleStyle]}>{this.props.titleNext}</Text>
                            </View>
                        </TouchableOpacity>

                    </View>
                </View>
            </View>

        )
    }
}


const styles = StyleSheet.create({
    // imgsrc: {
    //     flex: 1,
    //     width: null,
    //     height: null,
    //     justifyContent: 'center',
    //     alignItems: 'center',
    // },
    // title: {
    //     color: 'white',
    //     fontWeight: 'bold',
    //     fontSize: 22,
    //     backgroundColor: 'rgba(0,0,0,0)',
    // }


})