import React, { Component } from 'react';
import { View, Text, TextInput, StyleSheet, PickerIOS, Platform, Image, AsyncStorage, ScrollView, TouchableOpacity, Dimensions } from 'react-native';
import { Card, CardItem, Item, Picker, Textarea, Header, Left, Button, Body, Title, Right, Root, Toast } from 'native-base'
import Prepare_proposal_Styles from '../../Screens/Prepare_proposal_Styles';
import RNPickerSelect from 'react-native-picker-select';
import MyStyle from '../../Styles/MyStyle';
import { Actions } from 'react-native-router-flux';
import Spinner from 'react-native-loading-spinner-overlay';
import FooterNextButton from '../../CommonComponents/ProposalFooter/NextButton';
import DateTimePicker from 'react-native-modal-datetime-picker';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const feedbackForm = '';
const q1 = '';
const q2 = '';
const q3 = '';
const q4 = '';
const q5 = '';
const q6 = '';
export default class Buyer5 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pOne: '',
            pTwo: '',
            textAreaValue: '',
            textAreaValue1: '',
            textValue: '',
            PickerValue2: '',
            datePOne: '',
            StartDateVisible: false,
            StartDateVisibleTwo: false,
            items: [
                {
                    label: 'Yes',
                    value: 'Yes',
                },
                {
                    label: 'No',
                    value: 'No',
                },
            ],
            visible: false,
            question1: 'Have you met or Talked with this Seller?',
            question2: 'How did it go?',
            // 
            MetWithSeller: '',
            flagMetSeller: false,
            FeedbackData: this.props.clientDetail,
            pAddress: '',
            bACom: '',
            tCom: '',
        }
        // this.pickerfromAndroid = this.pickerfromAndroid.bind(this)
        this.onValueChange = this.onValueChange.bind(this);
    }

    _showDateOne = () => this.setState({ StartDateVisible: true, });
    _hideDateOne = () => this.setState({ StartDateVisible: false, });
    _handleDateOne = (date) => {
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let Dates = date.getDate();
        let selectedvalue = (month <= 9 ? '0' + month : month) + '/' + (Dates <= 9 ? '0' + Dates : Dates) + '/' + year;
        console.log('Start date : ', selectedvalue);
        q4 = '(Estimated closing date) ' + selectedvalue;
        this.setState({ datePOne: selectedvalue })
        this._hideDateOne();
    };



    onValueChange(number, value) {
        if (number == 1) {
            this.setState({ pOne: value });
            // alert(value);
            if (value == 'Yes') {
                q1 = '(Still in contract?) Yes';
            }
            else {
                q1 = '(Still in contract?) No';
            }
        }
        if (number == 2) {
            this.setState({ pTwo: value });
            // alert(value);
            if (value == 'Yes') {
                q2 = '(Are you representing the seller as well) Yes';
            }
            else {
                q2 = '(Are you representing the seller as well) No';
            }
        }

    }

    submitData() {

        // alert(this.state.PickerValue1)
        console.log("pOne => " + JSON.stringify(this.state.pOne));

        // console.log("datePOne => " + JSON.stringify(this.state.datePOne));
        // console.log("StartDateVisible => " + JSON.stringify(this.state.StartDateVisible));
        // console.log("datePTwo => " + JSON.stringify(this.state.datePTwo));
        // console.log("StartDateVisibleTwo => " + JSON.stringify(this.state.StartDateVisibleTwo));
        if (this.state.pOne == '') {
            Toast.show({ text: "Please, select Still in contract?", duration: 3000, });
        }
        if (this.state.pOne == 'Yes') {
            if (this.state.textAreaValue == '' || this.state.datePOne == '' || this.state.textValue == '' || this.state.pTwo == '') {
                Toast.show({ text: "Please, select all the fields", duration: 3000, });
            }
            else {
                q3 = '(Purchase price)' + this.state.textAreaValue;
                q5 = '(Escrow officer email)' + this.state.textValue;
                q6 = '(What happened?)' + this.state.textAreaValue1;

                var q7 = '(Purchase address)' + this.state.pAddress;
                var q8 = '(Buyer agent commission)' + this.state.bACom;
                var q9 = '(Total commission)' + this.state.tCom;
                console.log("feedbackForm ==> " + q1 + " " + q3 + " " + q4 + " " + q5 + " " + q2 + " " + q6);
                AsyncStorage.getItem('QuoteId').then((QuoteId) => {
                    this.setState({ visible: true });
                    let formdata = new FormData();
                    formdata.append("fromApp", true);
                    formdata.append("quoteId", QuoteId);
                    formdata.append("agentNote", q1 + q3 + q7 + q8 + q9 + q4 + q5 + q6 + q2);
                    // formdata.append("interviewDateStr", q4);
                    // formdata.append("spoken", this.state.PickerValue1);
                    fetch(global.BaseUrl + '/agentUpdateBuy-json', {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'multipart/form-data'
                        },
                        body: formdata,
                    }).then((response) => response.json())
                        .then((responseJson) => {
                            this.setState({ visible: false });
                            if (responseJson.msg == undefined) {
                                console.log("feedback seller 1 ==> " + JSON.stringify(responseJson));
                                Actions.ClientUpdate();
                            }
                            else {
                                Toast.show({ text: responseJson.msg, duration: 3000 });
                            }

                        })
                        .catch((error) => {
                            console.error(error);
                            this.setState({ visible: false });
                            Toast.show({ text: "Sorry, please try again.", duration: 3000 });
                        });
                }).done()
            }
        }



    }
    render() {
        const pickerStyle = {
            inputIOS: {
                color: '#3C444E',
                paddingTop: 13,
                paddingHorizontal: 10,
                paddingBottom: 12,
                fontWeight: 'bold',
                fontSize: 12
            },
            inputAndroid: {
                color: '#3C444E',
                fontWeight: 'bold',
                fontSize: 12
            },
            placeholderColor: '#3F4042',
            underline: { borderTopWidth: 0 },
            icon: {
                position: 'absolute',
                backgroundColor: 'transparent',
                borderTopWidth: 5,
                borderTopColor: '#00000099',
                borderRightWidth: 5,
                borderRightColor: 'transparent',
                borderLeftWidth: 5,
                borderLeftColor: 'transparent',
                width: 0,
                height: 0,
                top: 20,
                right: 15,
            },
        };
        return (
            <View style={{ backgroundColor: 'transparent', height: windowHeight - 73 }}>
                <Root>
                    <Spinner visible={this.state.visible} />
                    <ScrollView style={{ backgroundColor: 'transparent', height: windowHeight - 73 }}>
                        <View style={MyStyle.cardView}>
                            <Card style={{ marginBottom: 60 }}>
                                <CardItem>
                                    <View style={{ flex: 1 }}>
                                        <View style={[MyStyle.TopView, { justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }]}>
                                            <Text style={MyStyle.headingBoldtext}>We'd Like to hear From You</Text>
                                            <Text style={[MyStyle.headingNormalText, { textAlign: 'center' }]}>Regarding <Text style={{ color: '#0B78B7', textAlign: 'center' }}>{this.state.FeedbackData.name}</Text> selling <Text style={{ color: '#0B78B7', textAlign: 'center', alignSelf: 'center' }}>{this.state.FeedbackData.shortAddress}.</Text></Text>
                                        </View>
                                        <View style={Prepare_proposal_Styles.lineView} />
                                        <View>

                                            <Text style={[MyStyle.textStyles, { textAlign: 'left' }]}>Still in contract?</Text>
                                            <View style={MyStyle.PickerView}>
                                                <RNPickerSelect
                                                    placeholder={{ label: 'Select One Option', value: null, }}
                                                    items={this.state.items}
                                                    onValueChange={(value) => this.onValueChange(1, value)
                                                    }
                                                    style={pickerStyle}
                                                    value={this.state.pOne}
                                                />
                                            </View>
                                            {
                                                this.state.pOne == 'Yes' ?

                                                    <View>
                                                        <Text style={[MyStyle.textStyles, { textAlign: 'left' }]}>Purchase price:</Text>
                                                        <TextInput
                                                            placeholder='Purchase price'
                                                            placeholderTextColor='#7b7d82'
                                                            style={[MyStyle.textSingle,]}
                                                            keyboardType='numeric'
                                                            onChangeText={(text) => this.setState({ textAreaValue: text })}
                                                        />


                                                        <Text style={[MyStyle.textStyles, { textAlign: 'left' }]}>Purchase address:</Text>
                                                        <TextInput
                                                            placeholder='Purchase address'
                                                            placeholderTextColor='#7b7d82'
                                                            style={[MyStyle.textSingle,]}
                                                            // keyboardType='numeric'
                                                            onChangeText={(text) => this.setState({ pAddress: text })}
                                                        />

                                                        <Text style={[MyStyle.textStyles, { textAlign: 'left' }]}>Buyer agent commission:</Text>
                                                        <TextInput
                                                            placeholder='Buyer agent commission'
                                                            placeholderTextColor='#7b7d82'
                                                            style={[MyStyle.textSingle,]}
                                                            keyboardType='numeric'
                                                            onChangeText={(text) => this.setState({ bACom: text })}
                                                        />

                                                        <Text style={[MyStyle.textStyles, { textAlign: 'left' }]}>Total commission:</Text>
                                                        <TextInput
                                                            placeholder='Total commission'
                                                            placeholderTextColor='#7b7d82'
                                                            style={[MyStyle.textSingle,]}
                                                            keyboardType='numeric'
                                                            onChangeText={(text) => this.setState({ tCom: text })}
                                                        />
                                                        <Text style={[MyStyle.textStyles, { textAlign: 'left' }]}>Estimated closing date:</Text>
                                                        <TouchableOpacity onPress={this._showDateOne} style={{}}>
                                                            <Text style={MyStyle.datePickerTxt}>{this.state.datePOne}</Text>
                                                        </TouchableOpacity>
                                                        <DateTimePicker
                                                            isVisible={this.state.StartDateVisible}
                                                            onConfirm={this._handleDateOne}
                                                            onCancel={this._hideDateOne}
                                                        />

                                                        <Text style={[MyStyle.textStyles, { textAlign: 'left' }]}>Escrow officer email:</Text>
                                                        <TextInput
                                                            placeholder='Escrow officer email'
                                                            placeholderTextColor='#7b7d82'
                                                            style={[MyStyle.textSingle,]}
                                                            keyboardType='email-address'
                                                            onChangeText={(text) => this.setState({ textValue: text })}
                                                        // value={this.state.myNumber}
                                                        // maxLength={10}  //setting limit of input
                                                        />

                                                    </View>
                                                    :
                                                    <View>
                                                        <Text style={[MyStyle.textStyles, { textAlign: 'left' }]}>What happened?</Text>
                                                        <Textarea rowSpan={5} bordered
                                                            placeholder='Type your note here'
                                                            placeholderTextColor='#7b7d82'
                                                            style={[MyStyle.textArea,]}
                                                            onChangeText={(text) => this.setState({ textAreaValue1: text })}
                                                        />
                                                    </View>
                                            }

                                            {/* Default */}

                                            {/*  */}
                                            <Text style={[MyStyle.textStyles, { textAlign: 'left' }]}>Are you representing the seller as well?</Text>
                                            <View style={MyStyle.PickerView}>
                                                <RNPickerSelect
                                                    placeholder={{ label: 'Select One Option', value: null, }}
                                                    items={this.state.items}
                                                    onValueChange={(value) => this.onValueChange(2, value)
                                                    }
                                                    style={pickerStyle}
                                                    value={this.state.pTwo}
                                                />
                                            </View>
                                            {/* Default */}

                                        </View>
                                    </View>
                                </CardItem>
                            </Card>
                        </View>

                    </ScrollView>
                    <View style={{}}>
                        <FooterNextButton titleBack='UPDATE LATER' titleNext='SUBMIT' onPressNext={this.submitData.bind(this)} onPressBack={() => { Actions.pop() }} />
                    </View>
                </Root>
            </View>
        )
    }
}