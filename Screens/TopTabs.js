import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, AsyncStorage } from 'react-native';
import { Card, CardItem, Item, Tab, Tabs, TabHeading, Header, Picker, Textarea, Left, Button, Body, Title, Right } from 'native-base';
// import Prepare_proposal_Styles from './Prepare_proposal_Styles';
import Styless from './TabStyles';
import ClientTab from './ClientTab'
import MyStyle from '../Styles/MyStyle';
import { Actions } from 'react-native-router-flux';
import Referralfeetab from './ReferralFeeTab';
import { PrepareProposal11 } from './PrepareProposal11';
import { PrepareProposalTab } from './PrepareProposalTab';
var blueClient = require('../Images/blue_ico_client.png');
var whiteClient = require('../Images/white_ico_client.png');
var blueproposal = require('../Images/blue_ico_proposal.png');
var whiteProposal = require('../Images/white_ico_proposal.png');
var blueFeeicon = require('../Images/blue_ico_referral_fee.png');
var WhiteFeeicon = require('../Images/white_ico_referral_fee.png');

global.getTabVal = 0;
export class TopTabs extends Component {
    constructor() {
        super();
        this.state =
            {
                currentTab: '',
                // getTabVal: 1,
                clientTab: false,
                proposalTab: false,
                referralFeeTab: false
            }
    }
    componentWillMount() {
        AsyncStorage.setItem('QuoteId', JSON.stringify(this.props.ClientData.responseId));
        console.log("tab status ==> " + JSON.stringify(this.props.ClientData));
        console.log("clientType ==> " + JSON.stringify(this.props.ClientData.clientType));
        if (this.props.ClientData.responseStatus == 0 || this.props.ClientData.responseStatus == 1 || this.props.ClientData.responseStatus == 11) {
            // Referral Fee remains hidden
            this.setState({ clientTab: true, proposalTab: true })
        }
        else if (this.props.ClientData.responseStatus == 2 || this.props.ClientData.responseStatus == 3 || this.props.ClientData.responseStatus == 4) {
            // all tabs are visible
            this.setState({ clientTab: true, proposalTab: true, referralFeeTab: true })
        }
        else {
            this.setState({ clientTab: true });
        }
    }
    renderTab(index) {
        // console.log("index ==> " + index);
        const { params } = this.props.ClientData;

        if (index == 0) {
            return (
                <ClientTab {...this.props} {...params} />
            )
        }
        if (index == 1) {
            return (
                <PrepareProposalTab {...this.props} {...params} />
            )
        }
        if (index == 2) {
            return (
                <Referralfeetab {...this.props} {...params} />
            )
        }
    }
    tabChangeFunc(index) {
        // alert(index);
    }

    changeTap(index) {
        console.log("this.setState({ getTabVal: i }) ==> " + index)
        if (index == 0) {
            global.getTabVal = 0;
            setTimeout(() => {
                this.setState({ currentTab: 0 })
            }, 500);
        }
        if (index == 1) {
            global.getTabVal = 1;
            setTimeout(() => {
                this.setState({ currentTab: 0 })
            }, 500);
        }
        if (index == 2) {
            global.getTabVal = 2;
            setTimeout(() => {
                this.setState({ currentTab: 0 })
            }, 500);
        }
    }
    render() {
        // console.log("Tab Index => " + this.state.getTabVal);
        // let Clients = null;
        // let Proposal = null;
        // let Referral = null;
        // switch (this.state.getTabVal) {
        //     case 0:
        //     Clients = <View style={Style.inside_TabView}>
        //             {console.log("One " + this.state.getTabVal)}
        //             <Image source={this.state.getTabVal == '0' ? whiteClient : blueClient} style={{ height: 20, width: 20 }} />
        //             <Text style={this.state.getTabVal == '0' ? Style.activeTExtStyle : Style.textStyle}>Clients</Text>
        //         </View>
        //         break;
        //     case 1:
        //     Proposal = <View style={Style.inside_TabView}>
        //             {console.log("Two " + this.state.getTabVal)}
        //             <Image source={this.state.getTabVal == '1' ? whiteProposal : blueproposal} style={{ height: 20, width: 20 }} />
        //             <Text style={this.state.getTabVal == '1' ? Style.activeTExtStyle : Style.textStyle} >Proposal</Text>
        //         </View>
        //         break;
        //     case 2:
        //     Referral = <View style={Style.inside_TabView}>
        //             {console.log("Three " + this.state.getTabVal)}
        //             <Image source={this.state.getTabVal == '2' ? WhiteFeeicon : blueFeeicon} style={{ height: 20, width: 20 }} />
        //             <Text style={this.state.getTabVal == '2' ? Style.activeTExtStyle : Style.textStyle}>Referral Fee</Text>
        //         </View>
        //         break;
        //     default:
        //         return null;
        // }

        return (
            <View style={{ flex: 1 }}>
                <Header style={[MyStyle.headerParent]}>
                    <Left style={MyStyle.headerLR}>
                        <Button transparent onPress={() => { Actions.pop() }}>
                            <Image style={MyStyle.backImg} source={require('../Images/back_arrow.png')} />
                        </Button>
                    </Left>
                    <Body style={MyStyle.herderBody}>
                        <Title style={[MyStyle.whiteClr, MyStyle.fontBold, MyStyle.fontFamilyMB, MyStyle.font14]}>
                            {this.props.ClientData.name}
                        </Title>
                    </Body>
                    <Right style={MyStyle.headerLR}>
                    </Right>
                </Header>

                <Tabs
                    initialPage={global.getTabVal} onChangeTab={({ i, ref, from }) => this.changeTap(i)} tabBarUnderlineStyle={{ backgroundColor: '#689FD5' }} style={{ marginTop: -1 }}>
                    {this.state.clientTab
                        ?
                        <Tab heading={
                            <TabHeading style={{ backgroundColor: '#0e6dae' }} >
                                <View style={Style.inside_TabView}>
                                    {console.log("One " + global.getTabVal)}
                                    <Image source={global.getTabVal == 0 ? whiteClient : blueClient} style={{ height: 20, width: 20 }} />
                                    <Text style={global.getTabVal == 0 ? Style.activeTExtStyle : Style.textStyle}>Clients</Text>
                                </View>
                            </TabHeading>
                        }>
                            {this.renderTab(0)}
                        </Tab>
                        :
                        null
                    }
                    {this.state.proposalTab
                        ?
                        <Tab heading={
                            <TabHeading style={{ backgroundColor: '#0e6dae' }}>
                                <View style={Style.inside_TabView}>
                                    {console.log("Two " + global.getTabVal)}
                                    <Image source={global.getTabVal == 1 ? whiteProposal : blueproposal} style={{ height: 20, width: 20 }} />
                                    <Text style={global.getTabVal == 1 ? Style.activeTExtStyle : Style.textStyle} >Proposal</Text>
                                </View>
                            </TabHeading>
                        }>
                            {this.renderTab(1)}
                        </Tab>
                        :
                        null
                    }
                    {this.state.referralFeeTab
                        ?
                        <Tab heading={
                            <TabHeading style={{ backgroundColor: '#0e6dae' }}>
                                <View style={Style.inside_TabView}>
                                    {console.log("Three " + global.getTabVal)}
                                    <Image source={global.getTabVal == 2 ? WhiteFeeicon : blueFeeicon} style={{ height: 20, width: 20 }} />
                                    <Text style={global.getTabVal == 2 ? Style.activeTExtStyle : Style.textStyle}>Referral Fee</Text>
                                </View>
                            </TabHeading>
                        }>
                            {this.renderTab(2)}
                        </Tab>
                        :
                        null
                    }
                    {console.log("Tab Index => " + global.getTabVal)}
                </Tabs>

            </View>
        )
    }
}
const Style = StyleSheet.create(
    {
        inside_TabView:
        {
            flexDirection: 'column', alignItems: 'center', justifyContent: 'center'
        },
        activeTExtStyle:
        {
            color: '#fff',
            fontFamily: 'Montserrat-SemiBold'
        }
        ,
        textStyle:
        {
            color: '#74BBE5',
            fontFamily: 'Montserrat-SemiBold'
        }
    }
)

export default TopTabs;