import React, { Component } from 'react';
// import Footer from './Footer';
import Spinner from 'react-native-loading-spinner-overlay';
import { StyleSheet, Text, View, Image, FlatList, AsyncStorage, TouchableOpacity, StatusBar, Platform, Linking } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, CardItem, Card, Item, Input, Footer, FooterTab, Toast, Root } from 'native-base';
var MyStyle = require('../Styles/MyStyle');
import { Capitalize, bellFunc, messageFunc, settingFunc } from '../Js/MyScript';
import { ClosedLable, ExpiredLable, SoldLable, DeclinedLable, SubmittedLable, ExpiresSoonLable, InterviewingLable, WonLable, InvitedLable, ListedLable, PendingLable, PostponedLable, InContractLable, MatchedLable } from '../CommonComponents/ClientLabel';
import Mailer from 'react-native-mail';
import ActionTypes from '../Redux/Actions/ActionTypes';

global.filterData = false;
global.filterParameters = ''

global.sortData = false;
global.sortParameters = '';
global.getSellersBuyersValue = false; //Use true to get buyer requests, false for sellers. Default is false.
global.searchData = false
// global.sortData = false;

// 
global.searchSellBuy = ''
// LEO Redux
import { Actions } from '../../node_modules/react-native-router-flux';
import { connect } from 'react-redux';
const mapStateToProps = (state) => ({
    isLoading: state.serviceReducer.isLoading,
    error: state.serviceError.error,
    data: state.serviceReducer.data
});

const mapDispatchToProps = (dispatch) => ({
    callService: () => dispatch(origionalOnload())
});

// 

export class Clients extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: true,
            clientData: [],
            clientAddressCheck: '',
            paginationVal: 0,
            oneClickSellerFlag: '',
            oneClickBuyersFlag: ''
        }
        console.disableYellowBox = true;
        this.emailSend = this.emailSend.bind(this);
    }

    componentWillMount() {
        let formdata = new FormData();
        formdata.append("fromApp", true);
        fetch(global.BaseUrl + 'getAgentOneClickSubmitInfo-json', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            body: formdata,
        }).then((response) => response.json())
            .then((responseJson) => {
                this.setState({ oneClickSellerFlag: responseJson.canOneClickSubmitSell, oneClickBuyersFlag: responseJson.canOneClickSubmitBuy })
                console.log('oneclick ' + JSON.stringify(responseJson));
                // console.log('oneclick sell' + JSON.stringify(responseJson.canOneClickSubmitSell));
                // console.log('oneclick But' + JSON.stringify(responseJson.canOneClickSubmitBuy));
            })
            .catch((error) => {
                Toast.show({ text: "Sorry, please try again.", duration: 3000 });
            });
    }

    origionalOnload() {
        // return dispatch => {
        // dispatch(serviceActionPending());
        console.log('origional ==> ' + BaseUrl + 'getAgentRequests-json?fromApp=true&buy=' + global.getSellersBuyersValue);
        fetch(BaseUrl + 'getAgentRequests-json?offset=' + this.state.paginationVal + '&fromApp=true&buy=' + global.getSellersBuyersValue, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        }).then((response) => response.json())
            .then((responseJson) => {
                // IMP_C
                // console.log("responseJson ==> " + JSON.stringify(responseJson));
                this.setState({ clientData: responseJson.agentRequests, visible: false });
                // dispatch(serviceActionSuccess(esponseJson.agentRequests));
                // filter seller remove
                AsyncStorage.removeItem('SellerOpen');
                AsyncStorage.removeItem('SellerSubmitted');
                AsyncStorage.removeItem('SellerInterviewing');
                AsyncStorage.removeItem('SellerWon');
                AsyncStorage.removeItem('SellerListed');
                AsyncStorage.removeItem('SellerInContract');
                AsyncStorage.removeItem('SellerSold');

                AsyncStorage.removeItem('BuyerOpen');
                AsyncStorage.removeItem('BuyerSubmitted');
                AsyncStorage.removeItem('BuyerInterviewing');
                AsyncStorage.removeItem('BuyerWon');
                AsyncStorage.removeItem('BuyerInContract');
                AsyncStorage.removeItem('BuyerSold');

                // filter remove
                AsyncStorage.removeItem('SortNewRequests');
                AsyncStorage.removeItem('SortOldRequests');
                AsyncStorage.removeItem('SortFirstName');
                AsyncStorage.removeItem('SortLastName');
                AsyncStorage.removeItem('SortLastActiveOnline');
                AsyncStorage.removeItem('FilterSellersBuyers');


            })
            .catch((error) => {
                // dispatch(serviceActionError());
                console.error(error);
                this.setState({ visible: false });
                Toast.show({ text: "Sorry, please try again.", duration: 3000 });
            });
        // }
    }


    filterDataRecords() {
        console.log('filter ==> ' + BaseUrl + 'getAgentRequests-json?fromApp=true' + global.filterParameters);
        // fetch(BaseUrl + 'getAgentRequests-json?fromApp=true' + global.filterParameters, {
        fetch(BaseUrl + 'getAgentRequests-json?fromApp=true&buy=' + global.getSellersBuyersValue + global.filterParameters, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log("filter responseJson ==> " + JSON.stringify(responseJson));
                console.log("filter totalCount ==> " + JSON.stringify(responseJson.totalCount));
                if (responseJson.totalCount == 0) {
                    this.setState({ visible: false });
                    Toast.show({ text: "You don't have any clients under this category.", duration: 3000 });
                }
                else {
                    this.setState({ clientData: responseJson.agentRequests, visible: false });
                }

            })
            .catch((error) => {
                console.error(error);
                this.setState({ visible: false });
                Toast.show({ text: "Sorry, please try again.", duration: 3000 });
            });
    }

    sortDataRecords() {
        console.log('shortData ==> ' + BaseUrl + 'getAgentRequests-json' + global.sortParameters + '&offset=' + this.state.paginationVal);

        fetch(BaseUrl + 'getAgentRequests-json' + global.sortParameters + '&offset=' + this.state.paginationVal, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        }).then((response) => response.json())
            .then((responseJson) => {
                // console.log("responseJson ==> " + JSON.stringify(responseJson));
                this.setState({ clientData: responseJson.agentRequests, visible: false });
            })
            .catch((error) => {
                console.error(error);
                this.setState({ visible: false });
                Toast.show({ text: "Sorry, please try again.", duration: 3000 });
            });
    }

    componentDidMount() {
        if (global.filterData == true) {
            this.filterDataRecords();
        }
        else if (global.sortData == true) {
            this.sortDataRecords();
        }
        else {
            this.origionalOnload();
        }
    }
    // address check
    renderAddress(address) {
        // console.log(address);
        if (address != null || address != '') {
            return (
                <Text style={[MyStyle.cardText, { color: 'black' }]}>{address}</Text>
            )
        }
        else {
            return (<Text style={[MyStyle.cardText, { color: 'black' }]}>N/A</Text>)

        }
    }


    callTele(callNum) {
        if (Platform.OS === 'android') {
            Linking.canOpenURL("tel:" + callNum)
                .then((supported) => {
                    if (supported) {
                        return Linking.openURL("tel:" + callNum)
                            .catch(() => null);
                    }
                });
        } else {
            Linking.canOpenURL("telprompt:" + callNum)
                .then((supported) => {
                    if (supported) {
                        return Linking.openURL("telprompt:" + callNum)
                            .catch(() => null);
                    }
                });
        }
    }


    // render call
    renderCall(data) {
        // # Submitted
        if (data.responseStatus == 0 || data.responseStatus == 11) {
            if (data.contactInfoAvailable == true) {
                if (data.phone != '' || data.phone == true) {
                    return (
                        <TouchableOpacity onPress={() => { this.callTele(data.phone) }} style={MyStyle.CallTouch}>
                            <Image source={require('../Images/call.png')} style={MyStyle.callImg} />
                        </TouchableOpacity>
                    )
                }
            }
            else {
                if (data.pin == true || data.pin != '') {
                    return (
                        <TouchableOpacity onPress={() => { this.callTele(data.pin) }} style={MyStyle.CallTouch}>
                            <Image source={require('../Images/call.png')} style={MyStyle.callImg} />
                        </TouchableOpacity>
                    )
                }
            }
        }
        // # Interviewing
        if (data.responseStatus == 2) {
            if (data.contactInfoAvailable == true) {
                if (data.phone != '' || data.phone == true) {
                    return (
                        <TouchableOpacity onPress={() => { this.callTele(data.phone) }} style={MyStyle.CallTouch}>
                            <Image source={require('../Images/call.png')} style={MyStyle.callImg} />
                        </TouchableOpacity>
                    )
                }
                else {
                    if (data.pin != '' || data.pin == true) {
                        return (
                            <TouchableOpacity onPress={() => { this.callTele(data.pin) }} style={MyStyle.CallTouch}>
                                <Image source={require('../Images/call.png')} style={MyStyle.callImg} />
                            </TouchableOpacity>
                        )
                    }
                }
            }
        }

        // # Sell: Won, buy: Matched
        if (data.responseStatus == 3 && data.requestStatus != 9 && data.requestStatus != 13) {
            if (data.contactInfoAvailable == true) {
                if (data.phone != '' || data.phone == true) {
                    return (
                        <TouchableOpacity onPress={() => { this.callTele(data.phone) }} style={MyStyle.CallTouch}>
                            <Image source={require('../Images/call.png')} style={MyStyle.callImg} />
                        </TouchableOpacity>
                    )
                }
                else {
                    if (data.pin != '' || data.pin == true) {
                        return (
                            <TouchableOpacity onPress={() => { this.callTele(data.pin) }} style={MyStyle.CallTouch}>
                                <Image source={require('../Images/call.png')} style={MyStyle.callImg} />
                            </TouchableOpacity>
                        )
                    }
                }
            }
        }

        // # Listed (sell only)
        if (data.clientType == 'seller' && data.responseStatus == 3 && data.requestStatus == 9) {
            if (data.contactInfoAvailable == true) {
                if (data.phone != '' || data.phone == true) {
                    return (
                        <TouchableOpacity onPress={() => { this.callTele(data.phone) }} style={MyStyle.CallTouch}>
                            <Image source={require('../Images/call.png')} style={MyStyle.callImg} />
                        </TouchableOpacity>
                    )
                }
            }
        }

    }


    emailSend(ToEmail) {
        // console.log(PropertyImage);
        Mailer.mail({
            subject: 'UpNest',
            recipients: ['' + ToEmail + ''],
            isHTML: true, // iOS only, exclude if false
            // attachment: {
            //     path: PropertyImage,  // The absolute path of the file from which to read data.
            //     type: 'png',   // Mime Type: jpg, png, doc, ppt, html, pdf
            //     name: 'Ladunek',   // Optional: Custom filename for attachment
            // }
        }, (error, event) => {
            if (error) {
                Toast.show('Signup your email');
            }
        })
    }

    // render email
    renderEmail(data) {
        // // # Submitted
        // return (
        //     <Image source={require('../Images/email.png')} style={MyStyle.emailImg} />
        // )

        // # Submitted
        if (data.responseStatus == 0 || data.responseStatus == 11) {
            if (data.contactInfoAvailable == true) {
                if (data.email != undefined) {
                    return (
                        <TouchableOpacity onPress={() => { this.emailSend(data.email) }} style={MyStyle.emailTouch}>
                            <Image source={require('../Images/email.png')} style={MyStyle.emailImg} />
                        </TouchableOpacity>
                    )
                }
            }
            else {
                if (data.email != undefined) {
                    return (
                        <TouchableOpacity onPress={() => { this.emailSend(data.email) }} style={MyStyle.emailTouch}>
                            <Image source={require('../Images/email.png')} style={MyStyle.emailImg} />
                        </TouchableOpacity>
                    )
                }
            }
        }
        // # Interviewing
        if (data.responseStatus == 2) {
            if (data.contactInfoAvailable == true) {
                if (data.email != undefined) {
                    return (
                        <TouchableOpacity onPress={() => { this.emailSend(data.email) }} style={MyStyle.emailTouch}>
                            <Image source={require('../Images/email.png')} style={MyStyle.emailImg} />
                        </TouchableOpacity>
                    )
                }
                else {
                    if (data.email != undefined) {
                        return (
                            <TouchableOpacity onPress={() => { this.emailSend(data.email) }} style={MyStyle.emailTouch}>
                                <Image source={require('../Images/email.png')} style={MyStyle.emailImg} />
                            </TouchableOpacity>
                        )
                    }
                }
            }
        }

        // # Sell: Won, buy: Matched
        if (data.responseStatus == 3 && data.requestStatus != 9 && data.requestStatus != 13) {
            if (data.contactInfoAvailable == true) {
                if (data.email != undefined) {
                    return (
                        <TouchableOpacity onPress={() => { this.emailSend(data.email) }} style={MyStyle.emailTouch}>
                            <Image source={require('../Images/email.png')} style={MyStyle.emailImg} />
                        </TouchableOpacity>
                    )
                }
                else {
                    if (data.email != undefined) {
                        return (
                            <TouchableOpacity onPress={() => { this.emailSend(data.email) }} style={MyStyle.emailTouch}>
                                <Image source={require('../Images/email.png')} style={MyStyle.emailImg} />
                            </TouchableOpacity>
                        )
                    }
                }
            }
        }

        // # Listed (sell only)
        if (data.clientType == 'seller' && data.responseStatus == 3 && data.requestStatus == 9) {
            if (data.contactInfoAvailable == true) {
                if (data.email != undefined) {
                    return (
                        <TouchableOpacity onPress={() => { this.emailSend(data.email) }} style={MyStyle.emailTouch}>
                            <Image source={require('../Images/email.png')} style={MyStyle.emailImg} />
                        </TouchableOpacity>
                    )
                }
            }
        }
    }

    // estimate Line1
    renderEstimate(responseStatus, lowZestimate, highZestimate, clientType, quoteSubmittedDateStr, listDateStr, listPriceStr, finalSalesPriceStr, soldDateStr, soldPriceStr, requestStatus, requestCreatedDateStr) {
        // # Pending Line1
        if (responseStatus == 10) {
            if (clientType == 'seller') {
                return (
                    <Text style={[MyStyle.cardText]}>Estimate <Text style={MyStyle.dateColor}>{lowZestimate} - {highZestimate}</Text></Text>
                )
            }
            else {
                return (
                    <Text style={[MyStyle.cardText]}>Price Range {lowZestimate} - {highZestimate}</Text>
                )
            }
        }
        // # Submitted Line1
        if (responseStatus == 0 || responseStatus == 11) {
            if (clientType == 'seller') {
                return (
                    <Text style={[MyStyle.cardText]}>Estimate <Text style={MyStyle.dateColor}>{lowZestimate} - {highZestimate}</Text></Text>
                )
            }
            else {
                return (
                    <Text style={[MyStyle.cardText]}>Price Range {lowZestimate} - {highZestimate}</Text>
                )
            }

        }

        // # Interviewing
        if (responseStatus == 2) {
            if (clientType == 'seller') {
                return (
                    <Text style={[MyStyle.cardText]}>Estimate <Text style={MyStyle.dateColor}>{lowZestimate} - {highZestimate}</Text></Text>
                )
            }
            else {
                return (
                    <Text style={[MyStyle.cardText]}>Price Range {lowZestimate} - {highZestimate}</Text>
                )
            }
        }

        // # Sell: Won, buy: Matched
        if (responseStatus == 3 && requestStatus != 9 && requestStatus != 13) {
            if (clientType == 'seller') {
                return (
                    <Text style={[MyStyle.cardText]}>Estimate <Text style={MyStyle.dateColor}>{lowZestimate} - {highZestimate}</Text></Text>
                )
            }
            else {
                return (
                    <Text style={[MyStyle.cardText]}>Price Range {lowZestimate} - {highZestimate}</Text>
                )
            }
        }

        // # Listed (sell only)
        if (clientType == 'seller' && responseStatus == 3 && requestStatus == 9) {
            return (
                <Text style={[MyStyle.cardText]}>Listed on <Text style={MyStyle.dateColor}>{listDateStr}</Text> @ <Text style={MyStyle.dateColor}>{listPriceStr}</Text></Text>
            )
        }
        // else {
        //     return (
        //         null
        //     )
        // }

        // # In Contract

        if (responseStatus == 3 && (clientType == 'seller' && requestStatus == 13) || (clientType == 'buyer' && requestStatus == 9)) {
            return (
                <Text style={[MyStyle.cardText]}>Final Sales Price {finalSalesPriceStr} </Text>
            )
        }
        // # Sold
        if (responseStatus == 4) {
            if (clientType == 'seller') {
                return (
                    <Text style={[MyStyle.cardText]}>Sold on <Text style={MyStyle.dateColor}>{soldDateStr}</Text> @ <Text style={MyStyle.dateColor}>{soldPriceStr}</Text></Text>
                )
            }
            else {
                return (
                    <Text style={[MyStyle.cardText]}>Bought on <Text style={MyStyle.dateColor}>{soldDateStr}</Text> @ <Text style={MyStyle.dateColor}>{soldPriceStr}</Text></Text>
                )
            }
        }
    }

    // InviteSubmit Line2
    InvitedSubmittedLast_LoginListingBuyer(data) {
        // # Pending Line2
        if (data.responseStatus == 10) {
            return (
                <Text style={[MyStyle.cardText, MyStyle.fontFamilyMR, MyStyle.inviteSubmit]}>Invited on {data.requestCreatedDateStr}</Text>
            )
        }

        // # Submitted Line2
        if (data.responseStatus == 0 || data.responseStatus == 11) {

            return (
                <Text style={[MyStyle.cardText, MyStyle.fontFamilyMR, MyStyle.inviteSubmit]}>Submitted on {data.quoteSubmittedDateStr}</Text>
            )

        }
        if (data.responseStatus == 2) {
            return (
                <Text style={[MyStyle.cardText, MyStyle.fontFamilyMR, MyStyle.inviteSubmit]}>Submitted on {data.quoteSubmittedDateStr}</Text>
            )
        }

        // # Sell: Won, buy: Matched Line2
        if (data.responseStatus == 3 && data.requestStatus != 9 && data.requestStatus != 13) {
            if (data.clientType == 'seller') {
                return (
                    '-'
                )
            }
            else {
                if (data.listDateStr != '' || data.listDateStr != null || data.listDateStr != false) {
                    return (
                        <Text style={[MyStyle.cardText, MyStyle.fontFamilyMR, MyStyle.inviteSubmit]}>Last Login {data.clientLastLoginStr}</Text>
                    )
                }
            }
        }

        // }

        // # Listed (sell only)
        if (data.clientType == 'seller' && data.responseStatus == 3 && data.requestStatus == 9) {
            return (
                <Text style={[MyStyle.cardText, MyStyle.fontFamilyMR, { color: 'black' }]}>Listing Commission {data.sellFinalComm} %</Text>
            )
        }

        // # In Contract
        if (data.responseStatus == 3 && ((data.clientType == 'seller' && data.requestStatus == 13) || (data.clientType == 'buyer' && data.requestStatus == 9))) {

            if (data.clientType == 'seller') {
                return (
                    <Text style={[MyStyle.cardText, MyStyle.fontFamilyMR, { color: 'black' }]}>Listing Commission {data.sellFinalComm} %</Text>
                )
            }
            else {
                return (
                    <Text style={[MyStyle.cardText, MyStyle.fontFamilyMR, { color: 'black' }]}>Buyer Agent Commission {data.buyFinalComm} %</Text>
                )
            }
        }

        // # In Sold
        if (data.responseStatus == 4) {
            // if(data.clientType == 'seller') {
            if (data.referralFeePaid != true) {
                return (
                    <Text style={[MyStyle.cardText, MyStyle.fontFamilyMR, { color: 'black' }]}><Text style={MyStyle.refrlBlue}>{data.upnestFeeStr}</Text> Referral Fee Owed</Text>
                )
            }
            else {
                return (
                    <Text style={[MyStyle.cardText, MyStyle.fontFamilyMR, { color: 'black' }]}>{data.upnestFeeStr} Referral Fee Paid</Text>
                )
            }
        }
    }


    // renderPrepareProposal 
    renderPrepareProposal(data) {
        // console.log('data',data,this)
        // # Pending
        if (data.responseStatus == 10) {
            return (
                <Button style={MyStyle.PPBtn} onPress={this.goPrepareProposal.bind(this, data)}>
                    <Text style={[MyStyle.whiteClr, MyStyle.font11, MyStyle.fontBold, MyStyle.fontFamilyMR, { marginLeft: 7, marginRight: 7 }]}>Prepare Proposal</Text>
                </Button>
            )
        }

        // # Submitted
        if (data.responseStatus == 0 || data.responseStatus == 11) {
            return (
                <Button style={MyStyle.PPBtn}>
                    <Text style={[MyStyle.whiteClr, MyStyle.font11, MyStyle.fontBold, MyStyle.fontFamilyMR, { marginLeft: 7, marginRight: 7 }]}>View Proposal</Text>
                </Button>
            )
        }

        // # Interviewing
        if (data.responseStatus == 2) {
            return (
                <Button style={MyStyle.VDBtn}>
                    <Text style={[MyStyle.VDClr, MyStyle.font11, MyStyle.fontBold, MyStyle.fontFamilyMR, { marginLeft: 7, marginRight: 7 }]}>View Details</Text>
                </Button>
            )
        }

        // # Sell: Won, buy: Matched
        if (data.responseStatus == 3 && data.requestStatus != 9 && data.requestStatus != 13) {
            if (data.needListingAgreement) {
                return (
                    <Button style={MyStyle.PPBtn}>
                        <Text style={[MyStyle.whiteClr, MyStyle.font11, MyStyle.fontBold, MyStyle.fontFamilyMR, { marginLeft: 7, marginRight: 7 }]}>Upload Listing Agreement</Text>
                    </Button>
                )
            }
            else {
                return (
                    <Button style={MyStyle.VDBtn}>
                        <Text style={[MyStyle.VDClr, MyStyle.font11, MyStyle.fontBold, MyStyle.fontFamilyMR, { marginLeft: 7, marginRight: 7 }]}>View Details</Text>
                    </Button>
                )
            }
        }

        // # Listed (sell only)
        if (data.clientType == 'seller' && data.responseStatus == 3 && data.requestStatus == 9) {

            return (
                <Button style={MyStyle.VDBtn}>
                    <Text style={[MyStyle.VDClr, MyStyle.font11, MyStyle.fontBold, MyStyle.fontFamilyMR, { marginLeft: 7, marginRight: 7 }]}>View Details</Text>
                </Button>
            )
        }
        if (data.clientType == 'seller' && data.responseStatus == 3 && data.requestStatus == 9) {
            if (data.closingDoc == true) {
                return (
                    <Button style={MyStyle.PPBtn}>
                        <Text style={[MyStyle.whiteClr, MyStyle.font11, MyStyle.fontBold, MyStyle.fontFamilyMR, { marginLeft: 7, marginRight: 7 }]}>Update Escrow Info</Text>
                    </Button>
                )
            }
            else {
                return (
                    <Button style={MyStyle.PPBtn}>
                        <Text style={[MyStyle.whiteClr, MyStyle.font11, MyStyle.fontBold, MyStyle.fontFamilyMR, { marginLeft: 7, marginRight: 7 }]}>Submit Escrow Info</Text>
                    </Button>
                )
            }
        }

        // }

        // # In Contract
        if (data.responseStatus == 3 && ((data.clientType == 'seller' && data.requestStatus == 13) || (data.clientType == 'buyer' && data.requestStatus == 9))) {

            if (data.closingDoc == true) {
                return (
                    <TouchableOpacity>
                        <Button style={MyStyle.PPBtn}>
                            <Text style={[MyStyle.whiteClr, MyStyle.font11, MyStyle.fontBold, MyStyle.fontFamilyMR, { marginLeft: 7 }]}>Update Escrow Info</Text>
                        </Button>
                    </TouchableOpacity>
                )
            }
            else {
                return (
                    <TouchableOpacity>
                        <Button style={MyStyle.PPBtn}>
                            <Text style={[MyStyle.whiteClr, MyStyle.font11, MyStyle.fontBold, MyStyle.fontFamilyMR, { marginLeft: 7, marginRight: 7 }]}>Submit Escrow Info</Text>
                        </Button>
                    </TouchableOpacity>
                )
            }
        }

        // # Sold

        if (data.responseStatus == 4) {
            if (data.clientType == 'seller') {
                if (data.quickBooksUrl) {
                    return (
                        <TouchableOpacity>
                            <Button style={MyStyle.PPBtn}>
                                <Text style={[MyStyle.whiteClr, MyStyle.font11, MyStyle.fontBold, MyStyle.fontFamilyMR, { marginLeft: 7, marginRight: 7 }]}>Pay via QuickBooks</Text>
                            </Button>
                        </TouchableOpacity>
                    )
                }
                if (data.quickBooksUrl) {
                    if (data.needAlsoBuying == true) {
                        return (
                            <Button style={MyStyle.PPBtn}>
                                <Text style={[MyStyle.whiteClr, MyStyle.font11, MyStyle.fontBold, MyStyle.fontFamilyMR, { marginLeft: 7, marginRight: 7 }]}>Also Buying?</Text>
                            </Button>
                        )
                    }
                }
                else {
                    if (data.needAlsoBuying == true) {
                        return (
                            <Button style={MyStyle.PPBtn}>
                                <Text style={[MyStyle.whiteClr, MyStyle.font11, MyStyle.fontBold, MyStyle.fontFamilyMR, { marginLeft: 7, marginRight: 7 }]}>Also Buying?</Text>
                            </Button>
                        )
                    }
                }

            }
            else {
                if (data.quickBooksUrl) {
                    return (
                        <Button style={MyStyle.PPBtn}>
                            <Text style={[MyStyle.whiteClr, MyStyle.font11, MyStyle.fontBold, MyStyle.fontFamilyMR, { marginLeft: 7 }]}>Pay via QuickBooks</Text>
                        </Button>
                    )
                }
            }

            if (data.clientType == 'seller') {
                if (data.quickBooksUrl != true || data.quickBooksUrl != '' || data.quickBooksUrl != 'true') {
                    return (
                        <Button style={MyStyle.VDBtn}>
                            <Text style={[MyStyle.VDClr, MyStyle.font11, MyStyle.fontBold, MyStyle.fontFamilyMR, { marginLeft: 7, marginRight: 7 }]}>View Details</Text>
                        </Button>
                    )
                }
            }
            else {
                if (data.quickBooksUrl != true || data.quickBooksUrl != '' || data.quickBooksUrl != 'true') {
                    return (
                        <Button style={MyStyle.VDBtn}>
                            <Text style={[MyStyle.VDClr, MyStyle.font11, MyStyle.fontBold, MyStyle.fontFamilyMR, { marginLeft: 7, marginRight: 7 }]}>View Details</Text>
                        </Button>
                    )
                }
            }
        }

    }



    renderOneClickSubmit(data) {
        // console.log('Leo1 ' + global.getSellersBuyersValue);
        if (global.getSellersBuyersValue == false) {
            // console.log('Leo2 ' + this.state.oneClickSellerFlag);
            if (this.state.oneClickSellerFlag) {
                // console.log("Leo3 seller"); // seller
                if (data.responseStatus == 10) {
                    if (data.sample != true) {
                        return (
                            <Button style={MyStyle.Clk1Btn} onPress={this.goOneClickSubmit.bind(this, data)}>
                                <Text style={[MyStyle.whiteClr, MyStyle.font11, MyStyle.fontBold, MyStyle.fontFamilyMR, { marginLeft: 7 }]}>1 - Click Submit</Text>
                            </Button>
                        )
                    }
                }

            }
            // Seller
        }

        if (global.getSellersBuyersValue) {
            if (global.getSellersBuyersValue) {
                if (this.state.oneClickBuyersFlag) {
                    if (data.responseStatus == 10) {
                        if (data.sample != true) {
                            return (
                                <Button style={MyStyle.Clk1Btn} onPress={this.goOneClickSubmit.bind(this, data)}>
                                    <Text style={[MyStyle.whiteClr, MyStyle.font11, MyStyle.fontBold, MyStyle.fontFamilyMR, { marginLeft: 7 }]}>1 - Click Submit</Text>
                                </Button>
                            )
                        }
                    }
                }
                // buyer
            }
        }


    }

    // if clientType == 'seller':
    // }

    renderClientStatus(data) {
        // IMP_C
        console.log("clientStatus ==> " + data.displayStatus);

        if (data.displayStatus == 'Closed') {
            return (
                <ClosedLable />
            )
        }

        if (data.displayStatus == 'Postponed') {
            return (
                <PostponedLable />
            )
        }
        if (data.displayStatus == 'In Contract') {
            return (
                <InContractLable />
            )
        }
        if (data.displayStatus == 'Expired') {
            return (
                <ExpiredLable />
            )
        }
        if (data.displayStatus == 'Sold') {
            return (
                <SoldLable />
            )
        }

        if (data.displayStatus == 'Declined') {
            return (
                <DeclinedLable />
            )
        }

        if (data.displayStatus == 'Submitted') {
            return (
                <SubmittedLable />
            )
        }
        if (data.displayStatus == 'Expires Soon') {
            return (
                <ExpiresSoonLable />
            )
        }
        if (data.displayStatus == 'Interviewing') {
            return (
                <InterviewingLable />
            )
        }
        if (data.displayStatus == 'Won') {
            return (
                <WonLable />
            )
        }
        if (data.displayStatus == 'Listed') {
            return (
                <ListedLable />
            )
        }
        if (data.displayStatus == 'Invited') {
            return (
                <InvitedLable />
            )
        }
        if (data.displayStatus == 'Pending') {
            return (
                <PendingLable />
            )
        }

        if (data.displayStatus == 'Matched') {
            return (
                <MatchedLable />
            )
        }

    }



    renderClientsList(data) {
        // console.log("renderClientsList ==> " + JSON.stringify(data));

        return (
            <View key={data.highRange}>
                <Card style={{ borderRadius: 7 }}>
                    <TouchableOpacity onPress={() => { Actions.TopTabs({ ClientData: data }); }}>
                        <CardItem bordered style={{ borderRadius: 7 }}>
                            <Body style={{}}>

                                <View style={{ flexDirection: 'row', paddingRight: '22%' }}>

                                    <Text numberOfLines={1} style={[MyStyle.fontBold, MyStyle.fontFamilyMB, MyStyle.font15, { color: '#353c45', flex: 1, }]}>{Capitalize(data.name)}</Text>

                                </View>
                                {/* <View style={MyStyle.pendingView}><Text style={[MyStyle.fontFamilyMR, MyStyle.pendingTxt]}> Pending</Text></View> */}
                                {this.renderClientStatus(data)}
                                {/* <PendingLable  /> */}
                                {/* <KaushalLable  /> */}

                                {this.renderAddress(data.address)}

                                {/* Line1 */}
                                {this.renderEstimate(data.responseStatus, data.lowZestimate, data.highZestimate, data.clientType, data.quoteSubmittedDateStr, data.listDateStr, data.listPriceStr, data.finalSalesPriceStr, data.soldDateStr, data.soldPriceStr, data.requestStatus, data.requestCreatedDateStr)}

                                {/* Line2 */}
                                {this.InvitedSubmittedLast_LoginListingBuyer(data)}


                                <View style={{ flexDirection: 'row', marginTop: 7, backgroundColor: 'transparent', width: '100%' }}>
                                    {this.renderPrepareProposal(data)}
                                    {this.renderOneClickSubmit(data)}


                                    {this.renderCall(data)}
                                    {this.renderEmail(data)}
                                </View>

                            </Body>
                        </CardItem>
                    </TouchableOpacity>
                </Card>
            </View>
        )
    }

    reachedBottom() {
        // console.log("Page NO: => " + this.state.paginationVal);

        this.setState({ visible: true, paginationVal: this.state.paginationVal += 10 });
        console.log("Page NO: => " + this.state.paginationVal);

        var clientData = this.state.clientData;

        if (global.filterData == true) {
            console.log('filter ==> ' + BaseUrl + 'getAgentRequests-json?fromApp=true' + global.filterParameters);
            fetch(BaseUrl + 'getAgentRequests-json?fromApp=true' + global.filterParameters, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            }).then((response) => response.json())
                .then((data) => {
                    // console.log("responseJson ==> " + JSON.stringify(responseJson));
                    // this.setState({ clientData: responseJson.agentRequests, visible: false });
                    if (data.totalCount >= 10) {
                        data.agentRequests.map((ele) => {
                            clientData.push(ele)
                        })
                        this.setState({ clientData: clientData, visible: false });
                        console.log("filter load more ==> " + JSON.stringify(data));
                    }
                    else {
                        this.setState({ visible: false });
                    }

                })
                .catch((error) => {
                    console.error(error);
                    this.setState({ visible: false });
                    Toast.show({ text: "Sorry, please try again.", duration: 3000 });
                });
        }

        else if (global.sortData == true) {
            console.log('shortData ==> ' + BaseUrl + 'getAgentRequests-json?fromApp=true' + global.sortParameters + '&offset=' + this.state.paginationVal);

            fetch(BaseUrl + 'getAgentRequests-json' + global.sortParameters + '&offset=' + this.state.paginationVal, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            }).then((response) => response.json())
                .then((data) => {

                    if (data.totalCount >= 10) {
                        data.agentRequests.map((ele) => {
                            clientData.push(ele)
                        })
                        this.setState({ clientData: clientData, visible: false });
                    }
                    else {
                        this.setState({ visible: false });
                    }
                    // console.log("shortData responseJson ==> " + JSON.stringify(responseJson));
                    // this.setState({ clientData: responseJson.agentRequests, visible: false });
                })
                .catch((error) => {
                    console.error(error);
                    this.setState({ visible: false });
                    Toast.show({ text: "Sorry, please try again.", duration: 3000 });
                });
        }
        else if (global.searchData == true) {
            this.setState({ visible: false });
        }
        else {

            // 
            console.log('origional ==> ' + BaseUrl + 'getAgentRequests-json?fromApp=true&buy=' + global.getSellersBuyersValue);
            fetch(BaseUrl + 'getAgentRequests-json?offset=' + this.state.paginationVal + '&fromApp=true&buy=' + global.getSellersBuyersValue, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            }).then((response) => response.json())
                .then((data) => {
                    data.agentRequests.map((ele) => {
                        clientData.push(ele)
                    })
                    this.setState({ clientData: clientData, visible: false });
                    console.log("original ==> " + JSON.stringify(data));
                })
                .catch((error) => {
                    console.error(error);
                    this.setState({ visible: false });
                    Toast.show({ text: "Sorry, please try again.", duration: 3000 });
                });
        }

        // 


    }

    goPrepareProposal(data) {
        // Actions.PrepareProposal1({ ClientData: data.clientType })
        // alert("d f   "+ data.clientType);
        if (data.clientType == 'seller') {
            Actions.PrepareProposal1({ ClientData: data })
        }
        if (data.clientType == 'buyer') {
            Actions.BuyerProposal1({ ClientData: data })
        }
    }

    goOneClickSubmit(data) {
        console.log("oneClick ==> " + JSON.stringify(data.responseId));
        this.setState({ visible: true });
        let formdata = new FormData();
        formdata.append("fromApp", true);
        formdata.append("quoteId", data.responseId);
        fetch(global.BaseUrl + 'oneClickSubmitQuote-json', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            body: formdata,
        }).then((response) => response.json())
            .then((responseJson) => {
                this.setState({ visible: false });
                console.log("responseJson oneClick ==> " + JSON.stringify(responseJson.msg));
                if (responseJson.msg == undefined) {
                    Actions.PrepareProposal11({ ClientData: responseJson });
                }
                else {
                    Toast.show({ text: responseJson.msg, duration: 3000 });
                }
            })
            .catch((error) => {
                console.error(error);
                this.setState({ visible: false });
                Toast.show({ text: "Sorry, please try again.", duration: 3000 });
            });
        // }).done()



        // AsyncStorage.setItem('QuoteId', JSON.stringify(this.props.ClientData.responseId));
        // Actions.PrepareProposal11({ ClientData: data });
    }
    onSubmitHandler() {
        this.setState({ visible: true });

        if (this.state.searchClients == '') {
            this.setState({ visible: false });
            // this.setState({ paginationVal: 0 });
            this.origionalOnload();
        }
        global.searchData = true;
        console.log("Search Query " + global.BaseUrl + 'getAgentRequests-json?fromApp=true&searchQuery=' + this.state.searchClients + global.searchSellBuy);
        fetch(global.BaseUrl + 'getAgentRequests-json?fromApp=true&searchQuery=' + this.state.searchClients + global.searchSellBuy, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log("search result ==> " + JSON.stringify(responseJson));
                if (responseJson.totalCount == 0) {
                    this.setState({ visible: false });
                    Toast.show({ text: "You don't have any clients under this category.", duration: 3000 });
                }
                else {
                    this.setState({ clientData: responseJson.agentRequests, visible: false });
                }

            })
            .catch((error) => {
                // console.error(error);
                this.setState({ visible: false });
                Toast.show({ text: "Sorry, please try again.", duration: 3000 });
            });
    }

    render() {
        StatusBar.setBarStyle('light-content', true);
        return (
            <Container style={MyStyle.MainContainer}>
                <Root>
                    <Spinner visible={this.state.visible} />
                    <Header style={MyStyle.headerParent}>
                        <Left style={MyStyle.headerLR}>
                            <Button transparent>
                                {/* <Icon name='arrow-back' /> */}
                            </Button>
                        </Left>
                        <Body style={MyStyle.herderBody}>
                            <Title style={[MyStyle.whiteClr, MyStyle.fontBold, MyStyle.fontFamilyMB, MyStyle.font14]}>CLIENTS</Title>
                        </Body>
                        <Right style={MyStyle.headerLR}>
                            <Button transparent onPress={() => { Actions.FilterClient() }}>
                                <Image source={require('../Images/filter.png')} style={MyStyle.filterBtn} />
                            </Button>
                            <Button transparent onPress={() => { Actions.ShortClients() }}>
                                <Image source={require('../Images/sort.png')} style={MyStyle.filterBtn} />
                            </Button>
                        </Right>
                    </Header>
                    <Container style={{ backgroundColor: 'transparent', }}>
                        <View style={{ marginHorizontal: 10 }}>
                            <Item regular style={MyStyle.searchBarItems}>
                                <Image source={require('../Images/search.png')} style={MyStyle.searchBarImg} />
                                <Input placeholder='Search by Name, Email, Phone #, or Address' style={[MyStyle.fontFamilyML, MyStyle.searchBarInput]} onChangeText={(searchClients) => this.setState({ searchClients })} value={this.state.searchClients} returnKeyType="go" onSubmitEditing={(event) => this.onSubmitHandler(event)} />
                            </Item>
                        </View>

                        <FlatList
                            style={{ paddingHorizontal: 10 }}
                            data={this.state.clientData}
                            renderItem={({ item }) => (
                                this.renderClientsList(item)
                            )}
                            // keyExtractor={(rowData, index) => index}
                            // scrollToEnd={this.reachedBottom}
                            onEndReached={() => {
                                this.reachedBottom();
                            }}
                            onEndReachedThreshold={0.1}
                        />

                    </Container>
                    {/* <Footer /> */}

                    <Footer style={MyStyle.FooterParent}>
                        <FooterTab style={MyStyle.FooterTab}>
                            <Button onPress={() => { Actions.Clients() }}>
                                <Image source={require('../Images/home_selected.png')} style={[MyStyle.footerIconsHome]} />
                                <View style={[MyStyle.footerView]}>
                                </View>
                            </Button>

                            <Button onPress={() => { Actions.Alerts() }}>
                                <Image source={require('../Images/bell.png')} style={[MyStyle.footerIconsBell]} />
                            </Button>

                            <Button onPress={() => { Actions.ClientUpdate() }}>
                                <Image source={require('../Images/new-message.png')} style={[MyStyle.footerIconsMesg]} />
                            </Button>

                            <Button onPress={() => { Actions.Settings() }}>
                                <Image source={require('../Images/settings.png')} style={[MyStyle.footerIconsSetting]} />
                            </Button>
                        </FooterTab>
                    </Footer>
                </Root>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    containter: {
        flex: 1,
    }
});

export const serviceActionPending = () => ({
    type: ActionTypes.SERVICE_PENDING
})

export const serviceActionError = (error) => ({
    type: ActionTypes.SERVICE_ERROR,
    error: error
})

export const serviceActionSuccess = (data) => ({
    type: ActionTypes.SERVICE_SUCCESS,
    data: data
})

export default connect(mapStateToProps, mapDispatchToProps)(Clients);
