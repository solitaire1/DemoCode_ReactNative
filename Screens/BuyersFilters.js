import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, FlatList, TouchableOpacity, ScrollView, AsyncStorage, Dimensions } from 'react-native';
var MyStyle = require('../Styles/MyStyle');

class BuyersFilters extends Component {
    constructor(props) {
        super(props)
        this.state = {
            openBuyerTag: 'Open',
            openTick: false,
            openVal: '&statuses=Open',

            SubmittedBuyerTag: 'Submitted',
            SubmittedTick: false,
            SubmittedVal: '&statuses=Submitted',

            InterviewingBuyerTag: 'Interviewing',
            InterviewingTick: false,
            InterviewingVal: '&statuses=Interviewing',

            WonBuyerTag: 'Matched',
            WonTick: false,
            WonVal: '&statuses=Matched',

            InContractBuyerTag: 'In Contract',
            InContractTick: false,
            InContractVal: '&statuses=InContract',

            SoldBuyerTag: 'Sold',
            SoldTick: false,
            SoldVal: '&statuses=Sold',
        }
    }


    componentDidMount() {
        AsyncStorage.getItem('BuyerOpen').then((BuyerOpen) => {
            AsyncStorage.getItem('BuyerSubmitted').then((BuyerSubmitted) => {
                AsyncStorage.getItem('BuyerInterviewing').then((BuyerInterviewing) => {
                    AsyncStorage.getItem('BuyerWon').then((BuyerWon) => {
                        AsyncStorage.getItem('BuyerInContract').then((BuyerInContract) => {
                            AsyncStorage.getItem('BuyerSold').then((BuyerSold) => {

                                if (BuyerOpen != null) {
                                    this.setState({ openTick: true, })
                                }
                                if (BuyerSubmitted != null) {
                                    this.setState({ SubmittedTick: true, })
                                }
                                if (BuyerInterviewing != null) {
                                    this.setState({ InterviewingTick: true, })
                                }
                                if (BuyerWon != null) {
                                    this.setState({ WonTick: true, })
                                }
                                if (BuyerInContract != null) {
                                    this.setState({ InContractTick: true, })
                                }
                                if (BuyerSold != null) {
                                    this.setState({ SoldTick: true, })
                                }

                            })
                        })
                    })
                })
            })
        })
    }


    checkStatus(value) {
        if (value == 'Open') {
            this.setState({ openTick: true, })
            AsyncStorage.setItem('BuyerOpen', this.state.openVal);
            if (this.state.openTick == true) {
                this.setState({ openTick: false, })
                AsyncStorage.setItem('BuyerOpen', '');
            }
        }
        if (value == 'Submitted') {
            this.setState({ SubmittedTick: true, })
            AsyncStorage.setItem('BuyerSubmitted', this.state.SubmittedVal);
            if (this.state.SubmittedTick == true) {
                this.setState({ SubmittedTick: false, })
                AsyncStorage.setItem('BuyerSubmitted', '');
            }
        }

        if (value == 'Interviewing') {
            this.setState({ InterviewingTick: true, })
            AsyncStorage.setItem('BuyerInterviewing', this.state.InterviewingVal);
            if (this.state.InterviewingTick == true) {
                this.setState({ InterviewingTick: false, })
                AsyncStorage.setItem('BuyerInterviewing', '');
            }
        }

        if (value == 'Matched') {
            this.setState({ WonTick: true, })
            AsyncStorage.setItem('BuyerWon', this.state.WonVal);
            if (this.state.WonTick == true) {
                this.setState({ WonTick: false, })
                AsyncStorage.setItem('BuyerWon', '');
            }
        }

        if (value == 'In Contract') {
            this.setState({ InContractTick: true, })
            AsyncStorage.setItem('BuyerInContract', this.state.InContractVal);
            if (this.state.InContractTick == true) {
                this.setState({ InContractTick: false, })
                AsyncStorage.setItem('BuyerInContract', '');
            }
        }

        if (value == 'Sold') {
            this.setState({ SoldTick: true, })
            AsyncStorage.setItem('BuyerSold', this.state.SoldVal);
            if (this.state.SoldTick == true) {
                this.setState({ SoldTick: false, })
                AsyncStorage.setItem('BuyerSold', '');
            }
        }
    }
    render() {
        return (
            <View>
                <TouchableOpacity onPress={() => { this.checkStatus(this.state.openBuyerTag) }}>
                    <View style={MyStyle.filterView}>
                        <Text style={MyStyle.filterTxt}>{this.state.openBuyerTag}</Text>
                        {this.state.openTick == true ?
                            <Image source={require('../Images/check-mark.png')} style={MyStyle.filterCheckImg} />
                            :
                            null
                        }
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => { this.checkStatus(this.state.SubmittedBuyerTag) }}>
                    <View style={MyStyle.filterView}>
                        <Text style={MyStyle.filterTxt}>{this.state.SubmittedBuyerTag}</Text>
                        {this.state.SubmittedTick == true ?
                            <Image source={require('../Images/check-mark.png')} style={MyStyle.filterCheckImg} />
                            :
                            null
                        }
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => { this.checkStatus(this.state.InterviewingBuyerTag) }}>
                    <View style={MyStyle.filterView}>
                        <Text style={MyStyle.filterTxt}>{this.state.InterviewingBuyerTag}</Text>
                        {this.state.InterviewingTick == true ?
                            <Image source={require('../Images/check-mark.png')} style={MyStyle.filterCheckImg} />
                            :
                            null
                        }
                    </View>
                </TouchableOpacity>


                <TouchableOpacity onPress={() => { this.checkStatus(this.state.WonBuyerTag) }}>
                    <View style={MyStyle.filterView}>
                        <Text style={MyStyle.filterTxt}>{this.state.WonBuyerTag}</Text>
                        {this.state.WonTick == true ?
                            <Image source={require('../Images/check-mark.png')} style={MyStyle.filterCheckImg} />
                            :
                            null
                        }
                    </View>
                </TouchableOpacity>


                <TouchableOpacity onPress={() => { this.checkStatus(this.state.InContractBuyerTag) }}>
                    <View style={MyStyle.filterView}>
                        <Text style={MyStyle.filterTxt}>{this.state.InContractBuyerTag}</Text>
                        {this.state.InContractTick == true ?
                            <Image source={require('../Images/check-mark.png')} style={MyStyle.filterCheckImg} />
                            :
                            null
                        }
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => { this.checkStatus(this.state.SoldBuyerTag) }}>
                    <View style={MyStyle.filterView}>
                        <Text style={MyStyle.filterTxt}>{this.state.SoldBuyerTag}</Text>
                        {this.state.SoldTick == true ?
                            <Image source={require('../Images/check-mark.png')} style={MyStyle.filterCheckImg} />
                            :
                            null
                        }
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

export default BuyersFilters;
