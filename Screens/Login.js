/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */


// testagent+mark@upnest.com / testpassword
// testagent+stephen@upnest.com / testpassword
// testagent+jared@upnest.com / testpassword - for buy requests
// testagent+kevin@upnest.com / testpassword - also has buy requests
// testagent+craig@upnest.com / testpassword


import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View, KeyboardAvoidingView, Image, Dimensions, Keyboard, TouchableWithoutFeedback, AsyncStorage, Platform, TouchableOpacity, ScrollView
} from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
import { Container, Header, Content, Form, Item, Input, Label, Button, Toast, Root } from 'native-base';
var MyStyle = require('../Styles/MyStyle');
import Spinner from 'react-native-loading-spinner-overlay';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Actions } from '../../node_modules/react-native-router-flux';

import { connect } from 'react-redux'
import { AuthenticateUser } from '../Redux/Actions/Action'

// import PasswordInputText from 'react-native-hide-show-password-input';
let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
global.paddingTop = 0
export class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false,
            PlatformName: '',
            secureTextEntryFlag: true,
            eyesFlag: false,
            myFlag: false,
            keyboardFlag: false,
        }
        this.startApp();
        // this._onLoad(this.props.AuthenticateUser)
        this._keyboardDidShow = this._keyboardDidShow.bind(this);
        this._keyboardDidHide = this._keyboardDidHide.bind(this);
    }

    startApp() {
        AsyncStorage.getItem('UserEmail').then((UserEmail) => {
            AsyncStorage.getItem('UserPassword').then((UserPassword) => {
                console.log("UserEmail " + UserEmail + " UserPassword " + UserPassword);
                if (UserEmail != null || userEmail != undefined || UserPassword != null || UserPassword != undefined) {
                    Actions.Clients();
                }
            });
        });
    }

    componentDidMount() {
        // alert(JSON.stringify(Platform.OS));
        if (Platform.OS == 'android') {
            this.setState({ PlatformName: true })
        }
        else {
            this.setState({ PlatformName: false })
        }
    }
    showPswd() {
        // alert("df");
        this.setState({ eyesFlag: true, secureTextEntryFlag: false });
    }
    hidePswd() {
        // alert("df");
        this.setState({ eyesFlag: false, secureTextEntryFlag: true });
    }
    pswdOnchangeText(userPswd) {
        // console.log('userPswd ==> ' + userPswd)
        this.setState({ userPswd, myFlag: true });
        if (userPswd == '') {
            // alert("df");
            this.setState({ myFlag: false, eyesFlag: false })
        }
    }
    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow() {
        // this.callKey();
        global.paddingTop = -180
        this.setState({ keyboardFlag: true })
        console.log('Keyboard Shown');
    }

    _keyboardDidHide() {
        // this.callKey();
        global.paddingTop = 0
        this.setState({ keyboardFlag: false })
        console.log('Keyboard Hidden');
    }

    callKey() {
        this.setState({ keyboardFlag: true })
    }
    LoginUser() {
        if (this.state.userEmail == '' || this.state.userEmail == 'undefined' || this.state.userEmail == null) {
            Toast.show({ text: "Sorry, email address are required, please try again.", duration: 3000 });
        }
        // else if (reg.test(this.state.userEmail) === false) {
        //     Toast.show({ text: "Sorry, enter the valid email address, please try again.", duration: 3000 })
        // }
        else if (this.state.userPswd == '' || this.state.userPswd == 'undefined' || this.state.userPswd == null) {
            Toast.show({ text: "Sorry, password are required, please try again.", duration: 3000 })
        }
        else {
            this.setState({ visible: true });

            AsyncStorage.setItem('UserEmail', this.state.userEmail);
            AsyncStorage.setItem('UserPassword', this.state.userPswd);
            let formdata = new FormData();
            formdata.append("fromApp", true);

            formdata.append("email", this.state.userEmail);
            formdata.append("passwd", this.state.userPswd);

            // formdata.append("email", 'testagent+mark@upnest.com')
            // formdata.append("passwd", 'testpassword')

            // formdata.append("email", 'realtor0007@hotmail.com')
            // formdata.append("passwd", 'upnest')
            // console.log("global.BaseUrl ==> " + JSON.stringify(global.BaseUrl));
            fetch(global.BaseUrl + 'login-json', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Content-Type': 'multipart/form-data'
                },
                body: formdata,
            }).then((response) => response.json())
                .then((responseJson) => {
                    // this.props._submitLogin(responseJson);
                    console.log("responseJson ==> " + JSON.stringify(responseJson));
                    if (responseJson.redirectUrl == global.BaseUrl + 'getAgentRequests-json') {
                        this.setState({ visible: false });
                        Keyboard.dismiss();
                        Actions.Clients();
                    }
                    else if (responseJson.redirectUrl == 'agentDashboard') {
                        this.setState({ visible: false });
                        Keyboard.dismiss();
                        Actions.Clients();
                    }
                    else if (responseJson.redirectUrl == global.BaseUrl + 'getAgentRequests-json?fromApp=true') {
                        this.setState({ visible: false });
                        Keyboard.dismiss();
                        Actions.Clients();
                    }
                    else if (responseJson.redirectUrl == global.BaseUrl + 'getAgentRequests-json?fromApp=true&buy=false&statuses=null&statuses=null&statuses=Interviewing&statuses=null&statuses=null&statuses=null&statuses=null') {
                        this.setState({ visible: false });
                        Actions.Clients();
                    }
                    else if (responseJson && responseJson.redirectUrl) {
                        this.setState({ visible: false });
                        Keyboard.dismiss();
                        Actions.Clients();
                    }
                    else {
                        this.setState({ visible: false });
                        Toast.show({ text: "Sorry, Please check your email or password", duration: 3000 });
                    }
                })
                .catch((error) => {
                    console.error(error);
                    this.setState({ visible: false });
                    Toast.show({ text: "Sorry, please try again.", duration: 3000 });
                });
        }
    }

    render() {
        // const paddingTop = -180
        return (
            <View style={styles.container}>
                <Spinner visible={this.state.visible} />
                <Root>
                    <View style={{ flex: 0.40, backgroundColor: 'transparent' }}>
                        <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss(); }}>
                            <View>
                                <Image source={require('../Images/login_bg1.png')} resizeMode="cover" style={{ width: windowWidth, height: (Platform.OS === 'ios' && windowHeight === 812) ? windowHeight * 0.35 : windowHeight * 0.40 }} />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>

                    <View style={{ flex: 0.60, backgroundColor: 'transparent', marginTop: global.paddingTop }}>
                        {/* <KeyboardAvoidingView style={{ flex: 1, backgroundColor: 'transparent' }} behavior='' keyboardVerticalOffset={0}> */}
                        <View style={{ flex: 1 }}>
                            <View style={{ flex: 1, padding: 30, backgroundColor: 'white' }}>
                                <Item stackedLabel>
                                    <Label style={[{ marginLeft: 5, fontSize: 13 }, MyStyle.grayL, MyStyle.fontBold]}>EMAIL</Label>
                                    <Input style={{ color: '#607D8B' }} onChangeText={(userEmail) => this.setState({ userEmail })}
                                        keyboardType="email-address" value={this.state.userEmail} />
                                </Item>

                                <Item stackedLabel style={{ marginTop: 15, }}>
                                    <Label style={[{ marginLeft: 5, fontSize: 13 }, MyStyle.grayL, MyStyle.fontBold]}>PASSWORD</Label>
                                    <Input style={{ color: '#607D8B' }} onChangeText={(userPswd) => this.pswdOnchangeText(userPswd)} value={this.state.userPswd} secureTextEntry={this.state.secureTextEntryFlag} />

                                    {this.state.eyesFlag != this.state.myFlag ?
                                        <View style={MyStyle.eyesImgView}>
                                            <TouchableOpacity onPress={this.showPswd.bind(this)}>
                                                <Image source={require('../Images/pswdShow.png')} style={MyStyle.eyesImg} />
                                            </TouchableOpacity>
                                        </View>
                                        :
                                        null
                                    }
                                    {this.state.eyesFlag
                                        ?
                                        <View style={MyStyle.eyesImgView}>
                                            <TouchableOpacity onPress={this.hidePswd.bind(this)}>
                                                <Image source={require('../Images/pswdHide.png')} style={MyStyle.eyesImg} />
                                            </TouchableOpacity>
                                        </View>
                                        :
                                        null
                                    }
                                </Item>

                                <Button block style={{ height: 55, backgroundColor: '#62A0D3', marginTop: 15, borderRadius: 27 }} onPress={this.LoginUser.bind(this)}>
                                    <Text style={[MyStyle.whiteClr, MyStyle.fontFamilyMB, MyStyle.font16]}>LOGIN</Text>
                                </Button>

                                <TouchableOpacity onPress={() => { Actions.ForgotPassword() }}>
                                    <Text style={{ color: '#aeaeaf', fontWeight: 'bold', textAlign: 'right', marginTop: 15 }}>Forgot password?</Text>
                                </TouchableOpacity>

                                <View style={{ flexDirection: 'row', marginTop: 15, backgroundColor: 'transparent', }}>
                                    <View style={{ flex: 0.5, backgroundColor: 'transparent', flexDirection: 'row', justifyContent: 'center', }}>
                                        <Image source={require('../Images/touch_id.png')} style={{
                                            width: 22, height: 24, marginLeft: 10, justifyContent: 'center', alignSelf: 'center'
                                        }} />
                                        <Text style={[MyStyle.grayClr, { marginLeft: 10, fontSize: 12 }, MyStyle.fontBold]}>Login with {'\n'} Touch ID</Text>
                                    </View>

                                    <View style={{ flex: 0.5, backgroundColor: 'transparent', flexDirection: 'row', justifyContent: 'center' }}>
                                        <Image source={require('../Images/face_idG.png')} style={{
                                            width: 22, height: 22, marginLeft: 10, justifyContent: 'center', alignSelf: 'center'
                                        }} />
                                        <Text style={[MyStyle.grayClr, { marginLeft: 10, fontSize: 12 }, MyStyle.fontBold]}>Login with {'\n'} Face ID</Text>
                                    </View>
                                </View>

                                <View style={{ alignItems: 'center', backgroundColor: 'transparent', alignSelf: 'center' }}>
                                    <View style={{ marginTop: 15 }}>
                                        <Text style={[{ backgroundColor: 'transparent', fontSize: 13, color: '#aeaeaf', textAlign: 'center' }]}>Don't have an account yet?</Text>
                                        <Text style={[{ textAlign: 'center', fontSize: 13, color: '#aeaeaf' }]}>Visit our website and <Text style={[MyStyle.fontFamilyMR, { color: '#0B78B7' }]}>Sign Up</Text></Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        {/* </KeyboardAvoidingView> */}
                    </View>
                </Root>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
});

// export default Login;
//export default startup;
const mapStateToProps = (state) => ({
    AuthenticateUser: state.authentication
})
const mapDispatchToProps = dispatch => ({
    _submitLogin: (response) => {
        dispatch(AuthenticateUser(response));
    }
})
export default connect(mapStateToProps, mapDispatchToProps)(Login);