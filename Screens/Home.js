/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View, ImageBackground, Image, TouchableOpacity, AlertIOS, Alert
} from 'react-native';
import { Actions } from '../../node_modules/react-native-router-flux';
import TouchID from 'react-native-touch-id';
import Spinner from 'react-native-loading-spinner-overlay';
var MyStyle = require('../Styles/MyStyle');

const optionalConfigObject = {
    title: "Authentication Required", // Android
    color: "#e00606", // Android,
    fallbackLabel: "Show Passcode" // iOS (if empty, then label is hidden)
}

export class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false
        }
    }
    componentDidMount() {
        TouchID.authenticate('UpNest Login With Touch ID', optionalConfigObject)
            .then(success => {
                Actions.Clients();
            })
            .catch(error => {
                // AlertIOS.alert('UpNest Authentication Failed');
                // Alert.alert('UpNest Authentication Failed');
            });
    }

    // _pressHandler() {

    // }

    render() {
        return (
            <View style={styles.container}>
                <Spinner visible={this.state.visible} />
                <ImageBackground source={require('../Images/login_bg.png')} style={{ width: '100%', height: '100%' }}>
                    <View style={{ flex: 1, }}>
                        <View style={{ flex: 0.5, top: 75, }}>

                            <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center', alignItems: 'center' }}>
                                {/* <View style={{ flex: 0.5, backgroundColor: 'transparent', alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}> */}
                                <TouchableOpacity >
                                    <Image source={require('../Images/thumb_id.png')} style={MyStyle.LoginImgs} />
                                </TouchableOpacity>
                                {/* </View> */}

                            </View>
                        </View>
                        <View style={{ flex: 0.5, backgroundColor: 'transparent', }}>
                            <View style={{ flex: 1, backgroundColor: 'transparent', marginTop: 10 }}>
                                <View style={MyStyle.homeHalfBottom}>
                                    <Text style={[MyStyle.whiteClr, MyStyle.fontFamilyMR]}>Scaning...</Text>
                                    <Text style={[MyStyle.whiteClr, MyStyle.fontFamilyMB, { top: 10, fontSize: 14 }]}>Please wait for your finger to scan</Text>
                                </View>

                                <View style={[MyStyle.homeHalfBottom, { marginTop: 10 }]}>
                                    <TouchableOpacity onPress={() => { Actions.Login() }}>
                                        <Image source={require('../Images/btn_login_email.png')} style={MyStyle.loginBtn} />
                                    </TouchableOpacity>
                                </View>

                                <View style={[MyStyle.homeHalfBottom, { marginHorizontal: 70 }]}>
                                    <Text style={[MyStyle.whiteClr, { top: 10, fontSize: 16, textAlign: 'center' }]}>Don't have an account yet? Visit our website and <Text style={MyStyle.fontFamilyMB}>Sign Up</Text></Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </ImageBackground>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
});

export default Home;